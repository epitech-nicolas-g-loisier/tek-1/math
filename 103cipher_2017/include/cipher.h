/*
** EPITECH PROJECT, 2017
** cipher
** File description:
** Prototypes the functions used in 103cipher
*/

#ifndef CIPHER_
#define CIPHER_

void	encrypt_mes(char *, int **, int);

int	decrypt_mes(char *, int **, int);

void	print_msg(int *, int);

void	print_inv_matrix(double **, int);

void	print_matrix(int **);

int	**calc_key_matrix(char *, int);

double	**get_inverse(int **, int);

int	*get_tab_ascii(char *, int);

int	my_strlen(char *);

int	my_compute_square_root(int);

#endif
