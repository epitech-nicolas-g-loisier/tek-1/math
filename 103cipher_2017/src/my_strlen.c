/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** Counts length of string
*/

int	my_strlen(char const *str)
{
	int size = 0;

	while (str[size] != '\0')
		size++;
	return(size);
}
