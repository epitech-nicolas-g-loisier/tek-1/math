/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** Returns a square root if whole number
*/

int	my_compute_square_root(int nb)
{
	int root = 0;

	if (nb < 0 && nb > 2147395600)
		return (0);
	while (root * root < nb)
		root++;
	return (root);
}
