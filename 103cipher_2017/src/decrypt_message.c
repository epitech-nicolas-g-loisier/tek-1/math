/*
** EPITECH PROJECT, 2017
** decrypt_message
** File description:
** Decrypts a message using a key
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "cipher.h"

int	decrypt_line(int *msg, double **key, int pos, int size)
{
	int *new_values = malloc(sizeof(int) * size);
	int line = 0;
	int col = 0;

	memset(new_values, 0, sizeof(int) * size);
	while (col < size) {
		line = 0;
		while (line < size) {
			new_values[col] += ((msg[pos + line] * key[line][col])
					* 10 + 5) / 10;
			line = line + 1;
		}
		col = col + 1;
	}
	line = 0;
	while (line < size) {
		msg[pos + line] = new_values[line];
		line = line + 1;
	}
	free(new_values);
	return (pos + col);
}

int	get_crypt_msg_ent(char *msg)
{
	int count = 0;
	int size = 1;

	while (msg[count] != '\0') {
		if ((msg[count] < '0' || msg[count] > '9') && msg[count] != ' ')
			return (0);
		if (msg[count] == ' ')
			size = size + 1;
		count = count + 1;
	}
	return (size);
}

int	*get_crypt_msg(char *msg)
{
	int *ascii_mes;
	int count = 0;
	int size = get_crypt_msg_ent(msg);

	if (size == 0)
		return (NULL);
	ascii_mes = malloc(sizeof(int) * (size + 1));
	count = 0;
	size = 0;
	while (msg[count] != '\0') {
		ascii_mes[size] = atoi(&msg[count]);
		size = size + 1;
		while (msg[count] >= '0' && msg[count] <= '9')
			count = count + 1;
		while (msg[count] == ' ')
			count = count + 1;
	}
	ascii_mes[size] = -1;
	return (ascii_mes);
}

int	decrypt_mes(char *message, int **key_matrix, int size)
{
	int counter = 0;
	int *ascii_mes = get_crypt_msg(message);
	double **inv_key = get_inverse(key_matrix, size);

	if (ascii_mes == NULL) {
		write(2, "./103cipher: incorrect crypted message.\n", 40);
		return (84);
	}
	if (inv_key == NULL) {
		write(2, "Determinant is null, cannot decrypt message.\n", 45);
		free(ascii_mes);
		return (84);
	}
	while (ascii_mes[counter] != -1)
		counter = decrypt_line(ascii_mes, inv_key, counter, size);
	print_inv_matrix(inv_key, size);
	print_msg(ascii_mes, 1);
	free(ascii_mes);
	return (0);
}
