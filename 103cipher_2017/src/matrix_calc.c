/*
** EPITECH PROJECT, 2017
** 103cipher
** File description:
** Calc of matrix
*/

#include <stdlib.h>
#include "cipher.h"

int	*full_line(int	size, char *key, int *pos)
{
	int	*line = malloc(sizeof(int) * (size + 1));
	int	count = 0;

	while (count < size && key[count + *pos] != '\0') {
		line[count] = key[count + *pos];
		count++;
	}
	while (count < size) {
		line[count] = 0;
		count = count + 1;
	}
	line[count] = -1;
	*pos = *pos + count;
	return (line);
}

int	**calc_key_matrix(char *key, int size)
{
	int	length = my_strlen(key);
	int	count = 0;
	int	count_line = 0;
	int	**key_matrix = malloc(sizeof(int *) * (size + 1));

	while (count_line < size) {
		key_matrix[count_line] = full_line(size, key, &count);
		if (count > length)
			count = length;
		count_line++;
	}
	key_matrix[count_line] = malloc(sizeof(int) * (size + 1));
	count = 0;
	while (count < size + 1) {
		key_matrix[count_line][count] = -1;
		count = count + 1;
	}
	return (key_matrix);
}
