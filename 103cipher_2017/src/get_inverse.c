/*
** EPITECH PROJECT, 2017
** get_inverse
** File description:
** Inverses the matrix
*/

#include <stdlib.h>

double	**inverse_key_size_one(int **key)
{
	double **inv_key = malloc(sizeof(double *));

	if (key[0][0] == 0) {
		free(inv_key);
		return (NULL);
	}
	inv_key[0] = malloc(sizeof(double));
	inv_key[0][0] = 1.0 / (double)key[0][0];
	return (inv_key);
}

double	**inverse_key_size_two(int **key)
{
	double **inv_key = malloc(sizeof(double *) * 2);
	double delta = key[0][0] * key[1][1] - key[0][1] * key[1][0];

	if (delta == 0) {
		free(inv_key);
		return (NULL);
	}
	inv_key[0] = malloc(sizeof(double) * 2);
	inv_key[1] = malloc(sizeof(double) * 2);
	inv_key[0][0] = (double)key[1][1] / delta;
	inv_key[0][1] = (double)key[0][1] / delta * -1;
	inv_key[1][0] = (double)key[1][0] / delta * -1;
	inv_key[1][1] = (double)key[0][0] / delta;
	return (inv_key);
}

double	get_delta(int **key)
{
	double delta;

	delta = key[0][0] * (key[1][1] * key[2][2] - key[1][2] * key[2][1]);
	delta -= key[1][0] * (key[0][1] * key[2][2] - key[0][2] * key[2][1]);
	delta += key[2][0] * (key[0][1] * key[1][2] - key[0][2] * key[1][1]);
	return (delta);
}

double **inverse_key_size_three(int **key)
{
	double **inv_key = malloc(sizeof(double *) * 3);
	double delt = get_delta(key);

	if (delt == 0) {
		free(inv_key);
		return (NULL);
	}
	inv_key[0] = malloc(sizeof(double) * 3);
	inv_key[1] = malloc(sizeof(double) * 3);
	inv_key[2] = malloc(sizeof(double) * 3);
	inv_key[0][0] = (key[1][1] * key[2][2] - key[1][2] * key[2][1]) / delt;
	inv_key[0][1] = -(key[0][1] * key[2][2] - key[0][2] * key[2][1]) / delt;
	inv_key[0][2] = (key[0][1] * key[1][2] - key[0][2] * key[1][1]) / delt;
	inv_key[1][0] = -(key[1][0] * key[2][2] - key[1][2] * key[3][0]) / delt;
	inv_key[1][1] = (key[0][0] * key[2][2] - key[0][2] * key[3][0]) / delt;
	inv_key[1][2] = -(key[0][0] * key[1][2] - key[0][2] * key[1][0]) / delt;
	inv_key[2][0] = (key[1][0] * key[2][1] - key[1][1] * key[3][0]) / delt;
	inv_key[2][1] = -(key[0][0] * key[2][1] - key[0][1] * key[3][0]) / delt;
	inv_key[2][2] = (key[0][0] * key[1][1] - key[0][1] * key[1][0]) / delt;
	return (inv_key);
}

double	**get_inverse(int **key, int size)
{
	double **inv_key = NULL;
	double **(*inverse_key[3])(int **);
	int count = 0;

	inverse_key[0] = &inverse_key_size_one;
	inverse_key[1] = &inverse_key_size_two;
	inverse_key[2] = &inverse_key_size_three;
	while (count < 2) {
		if (size - 1 == count)
			inv_key = inverse_key[count](key);
		count = count + 1;
	}
	return (inv_key);
}
