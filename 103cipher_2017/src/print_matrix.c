/*
** EPITECH PROJECT, 2017
** print_matrix
** File description:
** Prints the key matrix
*/

#include <stdio.h>

void	print_inv_matrix(double **matrix, int size)
{
	int line = 0;
	int col = 0;

	printf("Key matrix :\n");
	while (line < size) {
		col = 0;
		while (col < size) {
			if (col > 0)
				printf("\t");
			printf("%.2f", matrix[line][col]);
			col = col + 1;
		}
		printf("\n");
		line = line + 1;
	}
}

void	print_matrix(int **matrix)
{
	int line = 0;
	int col = 0;

	printf("Key matrix :\n");
	while (matrix[line][0] != -1) {
		col = 0;
		while (matrix[line][col] != -1) {
			if (col > 0)
				printf("\t");
			printf("%d", matrix[line][col]);
			col = col + 1;
		}
		printf("\n");
		line = line + 1;
	}
}
