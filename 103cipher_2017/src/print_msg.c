/*
** EPITECH PROJECT, 2017
** print_msg
** File description:
** Prints the message
*/

#include <stdio.h>

void	print_msg(int *message, int action_id)
{
	int counter = 1;

	if (action_id == 0) {
		printf("\nEncrypted message :\n%d", message[0]);
		while (message[counter] != -1) {
			printf(" %d", message[counter]);
			counter = counter + 1;
		}
	} else {
		printf("\nDecrypted message :\n");
		counter = 0;
		while (message[counter] != -1) {
			printf("%c", (char)message[counter]);
			counter = counter + 1;
		}
	}
	printf("\n");
}
