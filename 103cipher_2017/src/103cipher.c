/*
** EPITECH PROJECT, 2017
** 103cipher
** File description:
** Encrypt or decrypt a message using a key
*/

#include <unistd.h>
#include <stdlib.h>
#include "cipher.h"

void	free_int_tab(int **matrix, char *str)
{
	int	lenght = my_strlen(str);
	int	size = my_compute_square_root(lenght);
	int	count = 0;

	while (count < size + 1) {
		free(matrix[count]);
		count++;
	}
	free(matrix);
}

void	check_argc(int argc)
{
	if (argc != 4) {
		write(2, "Incorrect usage : ./103cipher msg key nbr\n", 42);
		write(2, "msg : Message to be encrypted/decrypted\n", 40);
		write(2, "key : Key used to decrypt/encrypt the msg\n", 42);
		write(2, "nbr : 0 to encrypt and 1 to decrypt the msg\n", 44);
		exit(84);
	}
}

void	check_argv(char **argv)
{
	if (argv[3][0] != '0' && argv[3][0] != '1') {
		write(2, "Invalid value of arg3, try 0 or 1\n", 34);
		exit(84);
	}
	if (argv[1][0] == 0 || argv[2][0] == 0) {
		write(2, "The message and the key cannot be null\n", 39);
		exit(84);
	}
}

int	main(int argc, char **argv)
{
	int	**key_matrix;
	int size = 0;
	int status = 0;

	check_argc(argc);
	check_argv(argv);
	size = my_compute_square_root(my_strlen(argv[2]));
	key_matrix = calc_key_matrix(argv[2], size);
	if (argv[3][0] == '0')
		encrypt_mes(argv[1], key_matrix, size);
	else
		status = decrypt_mes(argv[1], key_matrix, size);
	free_int_tab(key_matrix, argv[2]);
	return (status);
}
