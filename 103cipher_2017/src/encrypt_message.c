/*
** EPITECH PROJECT, 2017
** 103cipher
** File description:
** encrypt a message
*/

#include <stdlib.h>
#include <string.h>
#include "cipher.h"

int	*get_tab_ascii(char *str, int size)
{
	int length = my_strlen(str);
	int	count = 0;
	int	*ascii_mes = malloc(sizeof(int) * (length + size + 1));

	while (count < length) {
		ascii_mes[count] = str[count];
		count = count + 1;
	}
	while (count % size != 0) {
		ascii_mes[count] = 0;
		count = count + 1;
	}
	ascii_mes[count] = -1;
	return (ascii_mes);
}

int	encrypt_line(int *msg, int **key, int pos, int size)
{
	int *new_values = malloc(sizeof(int) * size);
	int line = 0;
	int col = 0;

	memset(new_values, 0, sizeof(int) * size);
	while (key[0][col] != -1) {
		line = 0;
		while (key[line][col] != -1) {
			new_values[col] += msg[pos + line] * key[line][col];
			line = line + 1;
		}
		col = col + 1;
	}
	line = 0;
	while (key[line][0] != -1) {
		msg[pos + line] = new_values[line];
		line = line + 1;
	}
	free(new_values);
	return (pos + col);
}

void	encrypt_mes(char *message, int **key_matrix, int size)
{
	int counter = 0;
	int	*ascii_mes = get_tab_ascii(message, size);

	while (ascii_mes[counter] != -1)
		counter = encrypt_line(ascii_mes, key_matrix, counter, size);
	print_matrix(key_matrix);
	print_msg(ascii_mes, 0);
	free(ascii_mes);
}
