/*
** EPITECH PROJECT, 2017
** pong
** File description:
** Prototypes the functions needed in the project
*/

#ifndef PONG_
#define PONG_

int	check_encounter(double, double, double*);

double	get_float(char *);

double	*get_tab(char *, char *, char *);

double	*calc_vector(double *, double *);

double	*calc_nextpos(double *, double *, int);

#endif
