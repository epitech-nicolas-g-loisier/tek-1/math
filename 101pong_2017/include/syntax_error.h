/*
** EPITECH PROJECT, 2017
** syntax_error
** File description:
** Prototype of functions that check syntax
*/

#ifndef ERR_CHECK_
#define ERR_CHECK_

int	is_argc_correct(int);

int	is_arg_number(char *);

int	is_n_correct(char *);

int	check_syntax(int, char **);

#endif
