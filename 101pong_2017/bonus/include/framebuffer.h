/*
** EPITECH PROJECT, 2017
** framebuffer
** File description:
** Contains the framebuffer struct
*/

#ifndef BUFF_
#define BUFF_

typedef struct framebuffer
{
	unsigned int width;
	unsigned int heigth;
	sfUint8 *pixels;
} framebuffer_t;

#endif
