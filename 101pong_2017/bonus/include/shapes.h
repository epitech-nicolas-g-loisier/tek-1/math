/*
** EPITECH PROJECT, 2017
** shapes
** File description:
** Prototypes the shapes possible to draw
*/

#include <SFML/Graphics.h>
#include "framebuffer.h"

#ifndef SHAPES_
#define SHAPES_

void	my_put_pixel(framebuffer_t *, unsigned int, unsigned int, sfColor);

void	my_draw_rect(framebuffer_t *, sfVector2u, sfVector2u, sfColor);

#endif
