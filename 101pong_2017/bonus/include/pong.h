/*
** EPITECH PROJECT, 2017
** pong
** File description:
** Prototypes the functions used by pong
*/

#include <SFML/Graphics.h>
#include "framebuffer.h"
#include "item_pos.h"

#ifndef PONG_
#define PONG_

int	calc_next_sprite(item_pos_t *, item_pos_t *, int *);

void	update_texture(sfTexture *, framebuffer_t *, item_pos_t *,
			item_pos_t *);

void	check_isclosing(sfRenderWindow *, sfEvent *, sfVector2i);

void	check_input(item_pos_t *);

void	update_score(sfVector2i *, item_pos_t *, item_pos_t *, int *);

void	display_score(sfRenderWindow *, sfVector2i);

#endif
