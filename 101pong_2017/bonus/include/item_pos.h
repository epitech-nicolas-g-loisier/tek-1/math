/*
** EPITECH PROJECT, 2017
** item_pos
** File description:
** Struct containg the coords of the ball and the bats
*/

#ifndef ITEM_POS_
#define ITEM_POS_

typedef struct item_pos
{
	sfVector2u l_bat;
	sfVector2u r_bat;
	sfVector2u ball;
} item_pos_t;

#endif
