/*
** EPITECH PROJECT, 2017
** update_score
** File description:
** Adds a point to the score if the ball exits
*/

#include <SFML/Graphics.h>
#include "item_pos.h"

void	reset_speed(item_pos_t *speed_vector, int *ball_hit)
{
	*ball_hit = *ball_hit + 1;
	speed_vector->r_bat.y = 0;
	speed_vector->l_bat.y = 0;
}

void	update_score(sfVector2i *score, item_pos_t *pos, item_pos_t *add,
			int *ball_hit)
{
	unsigned int y_ball = pos->ball.y;

	add->ball.x = add->ball.x * -1;
	if (pos->ball.x > 400) {
		if (y_ball > pos->r_bat.y + 150 || y_ball < pos->r_bat.y) {
			score->x = score->x + 1;
			pos->ball.x = pos->r_bat.x - 75;
			pos->ball.y = pos->r_bat.y + 75;
			add->ball.x = -5;
		}
	} else {
		if (y_ball > pos->l_bat.y + 150 || y_ball < pos->l_bat.y) {
			score->y = score->y + 1;
			pos->ball.x = pos->l_bat.x + 75;
			pos->ball.y = pos->l_bat.y + 75;
			add->ball.x = 5;
		}
	}
	reset_speed(add, ball_hit);
}
