/*
** EPITECH PROJECT, 2017
** calc_next_sprite
** File description:
** Changes pos of ball
*/

#include <SFML/Graphics.h>
#include "item_pos.h"

int	verif_pos_bat(sfVector2u *position, sfVector2u *s_vector)
{
	if (s_vector->y > 5 && position->y < 5)
		return (1);
	if (s_vector->y == 5 && position->y > 445)
		return (1);
	position->y = position->y + s_vector->y;
	return (0);
}

int	verif_pos_ball(item_pos_t *pos, item_pos_t *s_vect)
{
	if (pos->ball.y < 10 || pos->ball.y > 589) {
		s_vect->ball.y = s_vect->ball.y * -1;
		if (pos->ball.y > 589)
			pos->ball.y = 589;
	}
	if (pos->ball.x < 50 || pos->ball.x > 745 - s_vect->ball.x)
		return (1);
	return (0);
}

void	increase_ball_speed(item_pos_t *s_vector)
{
	if (s_vector->ball.x > 0)
		s_vector->ball.x = s_vector->ball.x + 1;
	else
		s_vector->ball.x = s_vector->ball.x - 1;
	if (s_vector->ball.y > 0)
		s_vector->ball.y = s_vector->ball.y + 1;
	else
		s_vector->ball.y = s_vector->ball.y - 1;
}

int	calc_next_sprite(item_pos_t *position, item_pos_t *addition, int *b_hit)
{
	int check = 0;

	if (*b_hit > 5 && addition->ball.x < 10) {
		*b_hit = 0;
		increase_ball_speed(addition);
	}
	if (addition->ball.x == 5 || addition->ball.x == 4294967291) {
		if (addition->ball.y < 10)
			addition->ball.y = 4;
		else
			addition->ball.y = -4;
	}
	position->ball.x = position->ball.x + addition->ball.x;
	position->ball.y = position->ball.y + addition->ball.y;
	check = verif_pos_ball(position, addition);
	verif_pos_bat(&position->r_bat, &addition->r_bat);
	verif_pos_bat(&position->l_bat, &addition->l_bat);
	return (check);
}
