/*
** EPITECH PROJECT, 2017
** Pong
** File description:
** display score
*/

#include <SFML/Graphics.h>
#include <stdlib.h>

void	display_score(sfRenderWindow *window, sfVector2i score)
{
	sfText *player;
	sfFont	*font = sfFont_createFromFile("arial.ttf");
	char	score1[2] = {score.x + '0', 0};
	char	score2[2] = {score.y + '0', 0};

	player = sfText_create();
	sfText_setPosition(player, (sfVector2f){244, 0});
	sfText_setColor(player, sfRed);
	sfText_setCharacterSize(player, 150);
	sfText_setString(player, score1);
	sfText_setFont(player, font);
	sfRenderWindow_drawText(window, player, NULL);
	sfText_setPosition(player, (sfVector2f){500, 0});
	sfText_setColor(player, sfBlue);
	sfText_setString(player, score2);
	sfRenderWindow_drawText(window, player, NULL);
	sfFont_destroy(font);
	sfText_destroy(player);
}
