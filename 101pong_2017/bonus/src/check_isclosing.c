/*
** EPITECH PROJECT, 2017
** check_isclosing
** File description:
** Checks if the user requests the window to close
*/

#include <SFML/Graphics.h>

void	display_winner(char *player, sfRenderWindow *window, sfColor color)
{
	sfText *winner;
	sfFont *font = sfFont_createFromFile("arial.ttf");

	winner = sfText_create();
	sfText_setPosition(winner, (sfVector2f){65, 200});
	sfText_setString(winner, player);
	sfText_setFont(winner, font);
	sfText_setColor(winner, color);
	sfText_setCharacterSize(winner, 200);
	sfRenderWindow_drawText(window, winner, NULL);
	sfFont_destroy(font);
	sfText_destroy(winner);
}

void	check_isclosing(sfRenderWindow *windw, sfEvent *event, sfVector2i score)
{
	while (sfRenderWindow_pollEvent(windw, event)) {
		if (event->type == sfEvtClosed)
			sfRenderWindow_close(windw);
	}
	if (score.x == 9 || score.y == 9) {
		if (score.x == 9)
			display_winner("P1 won", windw, sfRed);
		else
			display_winner("P2 won", windw, sfBlue);
		sfRenderWindow_display(windw);
		while (sfRenderWindow_isOpen(windw)) {
			sfRenderWindow_pollEvent(windw, event);
			if (event->type == sfEvtClosed)
				sfRenderWindow_close(windw);
		}
	}
}
