/*
** EPITECH PROJECT, 2017
** check_input
** File description:
** Reads the input
*/

#include <SFML/Graphics.h>
#include "item_pos.h"

void	check_input(item_pos_t *addition)
{
	sfKeyCode input_list[5] = {sfKeyDown, sfKeyUp, sfKeyS, sfKeyZ};
	int counter = 0;

	while (counter < 2) {
		if (sfKeyboard_isKeyPressed(input_list[counter]))
			addition->r_bat.y += 5 * (1 - counter * 2);
		counter = counter + 1;
	}
	while (counter < 4) {
		if (sfKeyboard_isKeyPressed(input_list[counter]))
			addition->l_bat.y += 5 * (1 - counter % 2 * 2);
		counter = counter + 1;
	}
}
