/*
** EPITECH PROJECT, 2017
** draw_shape
** File description:
** Functions drawing a shape
*/

#include <SFML/Graphics.h>
#include "framebuffer.h"

void	my_put_pixel(framebuffer_t *buffer, unsigned int x, unsigned int y,
			sfColor color)
{
	buffer->pixels[(x + y * buffer->width) * 4] = color.r;
	buffer->pixels[(x + y * buffer->width) * 4 + 1] = color.g;
	buffer->pixels[(x + y * buffer->width) * 4 + 2] = color.b;
	buffer->pixels[(x + y * buffer->width) * 4 + 3] = color.a;
}

void	my_draw_rect(framebuffer_t *buffer, sfVector2u position,
			sfVector2u size, sfColor color)
{
	unsigned int x = 0;
	unsigned int y = 0;

	while (y < size.y) {
		my_put_pixel(buffer, position.x + x, position.y + y, color);
		x = x + 1;
		if (x >= size.x) {
			y = y + 1;
			x = x - size.x;
		}
	}
}
