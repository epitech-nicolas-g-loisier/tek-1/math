/*
** EPITECH PROJECT, 2017
** display_pixel
** File description:
** Displays pixel on a new window
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "framebuffer.h"
#include "item_pos.h"
#include "pong.h"

void	render_window(sfRenderWindow *window, framebuffer_t *fbuffer,
			sfTexture *texture, item_pos_t *addition)
{
	item_pos_t position = {{35, 250}, {750, 250}, {50, 200}};
	sfSprite *sprite = sfSprite_create();
	sfEvent event;
	sfVector2i score = {0, 0};
	int ball_hit = 0;

	sfSprite_setTexture(sprite, texture, sfTrue);
	while (sfRenderWindow_isOpen(window)) {
		check_isclosing(window, &event, score);
		check_input(addition);
		sfRenderWindow_clear(window, sfBlack);
		if (calc_next_sprite(&position, addition, &ball_hit)) {
			update_score(&score, &position, addition, &ball_hit);
		} else
			update_texture(texture, fbuffer, &position, addition);
		display_score(window, score);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		sfRenderWindow_display(window);
	}
	sfSprite_destroy(sprite);
}

void	framebuffer_destroy(framebuffer_t *framebuffer)
{
	free(framebuffer->pixels);
}

framebuffer_t	*framebuffer_create(unsigned int width, unsigned int heigth)
{
	static framebuffer_t framebuffer;
	sfUint8 *pixels;
	unsigned int counter = 0;

	pixels = malloc(width * heigth * 32 / 8);
	while (counter < width * heigth * 32 / 8) {
		pixels[counter] = 0;
		counter = counter + 1;
	}
	framebuffer.width = width;
	framebuffer.heigth = heigth;
	framebuffer.pixels = pixels;
	return (&framebuffer);
}

int	main(void)
{
	sfVideoMode mode = {800, 600, 32};
	sfRenderWindow *window;
	sfTexture *texture;
	item_pos_t addition = {{0, 0}, {0, 0}, {5, 4}};
	framebuffer_t *framebuffer;

	framebuffer = framebuffer_create(800, 600);
	window = sfRenderWindow_create(mode, "Pong", sfResize | sfClose, NULL);
	sfRenderWindow_setFramerateLimit(window, 60);
	texture = sfTexture_create(800, 600);
	render_window(window, framebuffer, texture, &addition);
	framebuffer_destroy(framebuffer);
	sfTexture_destroy(texture);
	sfRenderWindow_destroy(window);
	return (0);
}
