/*
** EPITECH PROJECT, 2017
** update_texture
** File description:
** Updates the texture
*/

#include <SFML/Graphics.h>
#include "framebuffer.h"
#include "item_pos.h"
#include "shapes.h"

void	draw_items(framebuffer_t *fbuffer, item_pos_t *item_pos, sfColor color)
{
	my_draw_rect(fbuffer, item_pos->l_bat, (sfVector2u){15, 150}, color);
	my_draw_rect(fbuffer, item_pos->r_bat, (sfVector2u){15, 150}, color);
	my_draw_rect(fbuffer, item_pos->ball, (sfVector2u){10, 10}, color);
}

void	update_texture(sfTexture *texture, framebuffer_t *fbuffer,
			item_pos_t *position, item_pos_t *addition)
{
	addition->r_bat.y = 0;
	addition->l_bat.y = 0;
	draw_items(fbuffer, position, sfWhite);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 800, 600, 0, 0);
	draw_items(fbuffer, position, sfTransparent);
}
