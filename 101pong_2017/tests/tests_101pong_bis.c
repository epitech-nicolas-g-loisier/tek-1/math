/*
** EPITECH PROJECT, 2017
** 101pong
** File description:
** Unit test
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(Pong, ball_not_reach_bat, .init = redirect_all_std)
{
	double	test[3] = {6.00, 6.00, -7.00};
	check_encounter(5, -2, test);
	cr_assert_stdout_eq_str("The ball won’t reach the bat.\n");
}
