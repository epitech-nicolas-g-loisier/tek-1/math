/*
** EPITECH PROJECT, 2017
** tests_101pong
** File description:
** Tests for 101pong
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "pong.h"
#include "syntax_error.h"

void	redirect_all_std(void);

Test(check_syntax, is_syntax_error_detected)
{
	cr_expect(is_argc_correct(3) == 84, "Wrong arg count not detected");
	cr_expect(is_argc_correct(8) == 0, "Correct arg count not detected");
	cr_expect(is_arg_number("12") == 0, "Int not detected");
	cr_expect(is_arg_number("-32") == 0, "Neg not detected");
	cr_expect(is_arg_number("83.65") == 0, "Float not detected");
	cr_expect(is_arg_number(".12") == 84, "No int detected");
	cr_expect(is_arg_number("62.1.7") == 84, "Too many dots");
	cr_expect(is_arg_number("-.65") == 84, "No int with neg");
	cr_expect(is_arg_number("-12.4") == 0, "Neg float");
	cr_expect(is_n_correct("-2") == 84, "neg counted as correct");
	cr_expect(is_n_correct("7") == 0, "int not detected");
	cr_expect(is_n_correct("0") == 0, "null counter as incorrect");
	cr_expect(is_n_correct("3.2") == 84, "float counted as correct");
}

Test(calc_vector, is_vector_correct, .init = redirect_all_std)
{
	double coord0[3];
	coord0[0] = 1.00;
	coord0[1] = 3.00;
	coord0[2] = 5.00;
	double coord1[3];
	coord1[0] = 7.00;
	coord1[1] = 9.00;
	coord1[2] = -2.00;
	calc_vector(coord0, coord1);
	cr_assert_stdout_eq_str("(6.00;6.00;-7.00)\n");
}

Test(calc_vector, is_vector_correct2)
{
	double coord0[3];
	coord0[0] = 1.1;
	coord0[1] = 3;
	coord0[2] = 5;
	double coord1[3];
	coord1[0] = -7;
	coord1[1] = 9;
	coord1[2] = 2;
	calc_vector(coord0, coord1);
	cr_assert("(-8.10;6.00;-3.00)\n");
}
