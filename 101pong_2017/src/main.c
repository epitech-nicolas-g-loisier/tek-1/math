/*
** EPITECH PROJECT, 2017
** Pong
** File description:
** main
*/

#include <stdlib.h>
#include "my.h"
#include "syntax_error.h"
#include "pong.h"

int	main(int ac, char **av)
{
	double	*t_point = NULL;
	double	*t_less_point = NULL;
	double	*s_vector = NULL;
	int	n = 0;

	n = check_syntax(ac, av);
	t_point = get_tab(av[1], av[2], av[3]);
	t_less_point = get_tab(av[4], av[5], av[6]);
	s_vector = calc_vector(t_point, t_less_point);
	free(t_point);
	calc_nextpos(t_less_point, s_vector, n);
	check_encounter(t_less_point[2], t_point[2], s_vector);
	free(t_point);
	free(t_less_point);
	free(s_vector);
	return (0);
}
