/*
** EPITECH PROJECT, 2017
** calc_nextpos
** File description:
** Calc the next pos of the ball
*/

#include <stdio.h>
#include <stdlib.h>

double	*calc_nextpos(double *oldpos, double *vector, int n)
{
	double *nextpos = NULL;
	int counter = n;

	nextpos = malloc(sizeof(double) * 4);
	nextpos[0] = oldpos[0];
	nextpos[1] = oldpos[1];
	nextpos[2] = oldpos[2];
	while (counter > 0) {
		nextpos[0] = nextpos[0] + vector[0];
		nextpos[1] = nextpos[1] + vector[1];
		nextpos[2] = nextpos[2] + vector[2];
		counter = counter - 1;
	}
	printf("At time t+%d, ball coordinates will be :\n", n);
	printf("(%.2f;%.2f;%.2f)\n", nextpos[0], nextpos[1], nextpos[2]);
	return (nextpos);
}
