/*
** EPITECH PROJECT, 2017
** calc_vector
** File description:
** Calculates the speed vector using given coordinates
*/

#include <stdio.h>
#include <stdlib.h>
#include "my.h"

double	*calc_vector(double *pos0, double *pos1)
{
	double *vect = NULL;
	int counter = 0;

	vect = malloc(sizeof(double) * 4);
	while (counter < 3) {
		vect[counter] = pos1[counter] - pos0[counter];
		counter = counter + 1;
	}
	printf("The speed vector coordinates are :\n");
	printf("(%.2f;%.2f;%.2f)\n", vect[0], vect[1], vect[2]);
	return (vect);
}
