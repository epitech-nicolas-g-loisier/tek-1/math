/*
** EPITECH PROJECT, 2017
** check_encounter
** File description:
** Checks if the ball will reach the bat
*/

#include <math.h>
#include <stdio.h>

int	check_encounter(double pos, double pos0, double *v_vector)
{
	double	angle = 0;
	double	square_xv = v_vector[0] * v_vector[0];
	double	square_yv = v_vector[1] * v_vector[1];
	double	square_zv = v_vector[2] * v_vector[2];
	double	norme_v = sqrt(square_xv + square_yv + square_zv);

	if ((pos < 0 && pos >= pos0) || (pos > 0 && pos <= pos0)) {
		printf("The ball won't reach the bat.\n");
		return (0);
	}
	printf("The incidence angle is :\n");
	angle = 90 - 180 / M_PI * acos(v_vector[2] / norme_v);
	if (angle < 0)
		angle = -angle;
	printf("%.2f degrees\n", angle);
	return (0);
}
