/*
** EPITECH PROJECT, 2017
** check_syntax
** File description:
** Checks for syntax errors and exit if needed
*/

#include <stdlib.h>
#include <stdio.h>
#include "my.h"

int	is_argc_correct(int argc)
{
	if (argc != 8) {
		fprintf(stderr, "Incorrect number of arg\n");
		exit(84);
	}
	return (0);
}

int	is_arg_number(char *str)
{
	int count = 0;
	int dot_counter = 0;

	if (str[0] == '-')
		count = count + 1;
	while (str[count] != '\0') {
		if ((str[count] < 48 || str[count] > 57) && str[count] != 46) {
			fprintf(stderr, "Syntax error\n");
			exit(84);
		}
		if (str[count] == '.')
			dot_counter = dot_counter + 1;
		count = count + 1;
	}
	if (dot_counter > 1 || str[0] == 46 || (str[1] == 46 && str[0] == 45)){
		fprintf(stderr, "Syntax error\n");
		exit(84);
	}
	return (0);
}

int	is_n_correct(char *n)
{
	int counter = 0;

	if (n[0] == '\0') {
		fprintf(stderr, "Invalid time shift\n");
		exit(84);
	}
	while (n[counter] != '\0') {
		if (n[counter] < '0' || n[counter] > '9') {
			fprintf(stderr, "Invalid time shift\n");
			exit(84);
		}
		counter = counter + 1;
	}
	return (my_getnbr(n));
}

int	check_syntax(int argc, char **argv)
{
	int counter = 1;

	is_argc_correct(argc);
	while (counter < 7) {
		is_arg_number(argv[counter]);
		counter = counter + 1;
	}
	counter = is_n_correct(argv[7]);
	return (counter);
}
