/*
** EPITECH PROJECT, 2017
** get_tab
** File description:
** Creates an array of floats using args
*/

#include <stdlib.h>
#include "pong.h"

double	*get_tab(char *str1, char *str2, char *str3)
{
	double *array = NULL;

	array = malloc(sizeof(double) * 4);
	array[0] = get_float(str1);
	array[1] = get_float(str2);
	array[2] = get_float(str3);
	array[3] = '\0';
	return (array);
}
