/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** Returns a number given as a string
*/

int	my_getnbr(char const *str)
{
	int return_v = 0;
	int argc = 0;
	int is_neg = 1;

	while (str[argc] > 57 || str[argc] < 48)
		argc = argc + 1;
	if (str[argc - 1] == '-')
		is_neg = -1;
	while (str[argc] < 58 && str[argc] > 47) {
		return_v = return_v * 10 + str[argc] - 48;
		if (return_v < 0 && is_neg == 1)
			return (0);
		if (return_v * is_neg > 0 && is_neg == -1)
			return (0);
		argc = argc + 1;
	}
	return (return_v * is_neg);
}
