/*
** EPITECH PROJECT, 2018
** calc_x
** File description:
** Replace the variable in the function by the value given as arg
*/

#include <math.h>

double	calc_x(double *coeff, double x)
{
	double value;

	value = coeff[0] + coeff[1] * x + coeff[2] * pow(x, 2);
	value = value + coeff[3] * pow(x, 3) + coeff[4] * pow(x, 4);
	return (value);
}
