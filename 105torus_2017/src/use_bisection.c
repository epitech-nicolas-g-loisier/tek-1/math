/*
** EPITECH PROJECT, 2018
** use_bisection
** File description:
** Uses bisection to find the answer
*/

#include <math.h>
#include <stdio.h>
#include "torus.h"

void	use_bisection(double *coefficient, int precision)
{
	double x1 = 0;
	double x2 = 1;
	double x = (x1 + x2) / 2;
	double prev;

	while (is_precision_reached(coefficient, precision, x) == 0) {
		if (calc_x(coefficient, x1) * calc_x(coefficient, x) < 0)
			x2 = x;
		else
			x1 = x;
		printf("x = %.*f\n", check_precision(x, precision), x);
		prev = x;
		x = (x1 + x2) / 2;
		if (prev == x)
			break;
	}
	printf("x = %.*f\n", precision, x);
}
