/*
** EPITECH PROJECT, 2018
** use_secant
** File description:
** Uses secant method to solve the equation
*/

#include <stdio.h>
#include "torus.h"

double	calc_adv_x(double current, double previous, double *coefficient)
{
	double value = 0;

	value = calc_x(coefficient, current) - calc_x(coefficient, previous);
	value = value / (current - previous);
	return (value);
}

void	use_secant(double *coeff, int precision)
{
	double prev = 0;
	double curr = 1;
	double next = 0;
	int loop_count = 0;

	while (is_precision_reached(coeff, precision, curr) == 0) {
		next = curr;
		next -= calc_x(coeff, curr) / calc_adv_x(curr, prev, coeff);
		printf("x = %.*f\n", check_precision(next, precision), next);
		prev = curr;
		curr = next;
		if (prev == curr)
			break;
		loop_count = loop_count + 1;
		if (loop_count > 50)
			break;
	}
	if (prev == 0 && curr == 1)
		printf("x = 1\n");
}
