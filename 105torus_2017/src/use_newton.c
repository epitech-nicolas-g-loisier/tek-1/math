/*
** EPITECH PROJECT, 2018
** use_newton
** File description:
** Uses the newton method to resolve the equation
*/

#include <math.h>
#include <stdio.h>
#include "torus.h"

void	use_newton(double *coefficient, int precision)
{
	double x = 0.5;
	double prev;

	while (is_precision_reached(coefficient, precision, x) == 0) {
		printf("x = %.*f\n", check_precision(x, precision), x);
		prev = x;
		x = x - calc_x(coefficient, x) / calc_deriv_x(coefficient, x);
		if (prev == x)
			break;
	}
	printf("x = %.*f\n", precision, x);
}
