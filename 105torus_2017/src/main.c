/*
** EPITECH PROJECT, 2017
** 105intersection
** File description:
** solve 4th degree equation
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "torus.h"
#include "my.h"

void	print_usage(void)
{
	printf("./105torus opt a0 a1 a2 a3 a4 n\n");
	printf("opt\t\tnumber of the option :\n");
	printf("\t\t\t1 for the bisection method,\n");
	printf("\t\t\t2 for Newton’s method,\n");
	printf("\t\t\t3 for the secant method\n");
	printf("a0,a1,a2,a3,a4\tcoefficients of the equation\n");
	printf("n\t\tprecision (meaning the application of the polynomial");
	printf("to the solution\n\t\tshould be smaller than 10-n)\n");
	exit(0);
}

void	print_err(void)
{
	write(2, "./105torus: incorrect arg\n", 26);
	write(2, "try again with -h\n", 18);
	exit(84);
}

void	check_method_type(char *type)
{
	if (my_getnbr(type) < 1 || my_getnbr(type) > 3)
		print_err();
}

void	check_arg(int argc, char **argv)
{
	int counter = 1;

	if (argc == 2 && my_strcmp(argv[1], "-h") == 0)
		print_usage();
	else if (argc != 8) {
		write(2, "./105torus: incorrect arg count\n", 32);
		write(2, "try again with -h\n", 18);
		exit(84);
	}
	while (counter < 8) {
		if (my_str_isnum(argv[counter]) == 0){
			print_err();
		}
		else if (counter == 7 && argv[counter][0] == '-'){
			print_err();
		}
		counter = counter + 1;
	}
	check_method_type(argv[1]);
}

int	main(int argc, char **argv)
{
	void	(*fptr[3]) (double *, int);
	double coeff[5];
	int counter = 0;

	check_arg(argc, argv);
	while (counter < 5) {
		coeff[counter] = my_getnbr(argv[counter + 2]);
		counter = counter + 1;
	}
	if (calc_x(coeff, 0) * calc_x(coeff, 1) > 0)
		return (84);
	fptr[0] = &use_bisection;
	fptr[1] = &use_newton;
	fptr[2] = &use_secant;
	fptr[(int)argv[1][0] - '1'](coeff, my_getnbr(argv[7]));
	return (0);
}
