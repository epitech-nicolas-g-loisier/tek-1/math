/*
** EPITECH PROJECT, 2018
** 105torus
** File description:
** check zero at the end of the number
*/

#include <math.h>

int	check_precision(double number, int precision)
{
	long nb = number * pow(10, precision);
	int check = 0;
	static int prec = 0;

	if (nb < 0)
		nb = -nb;
	while (nb != 0 && prec <= precision) {
		check = nb % 10;
		nb = nb / 10;
		if (check != 0)
			break;
		else
			precision--;
	}
	if (prec < precision)
		prec = precision;
	else
		prec += 1;
	return (precision);
}
