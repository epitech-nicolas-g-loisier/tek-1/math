/*
** EPITECH PROJECT, 2018
** calc_deriv_x
** File description:
** Calc the result of f'(x)
*/

#include <math.h>

double	calc_deriv_x(double *coeff, double x)
{
	double value;

	value = 4 * coeff[4] * pow(x, 3) + 3 * coeff[3] * pow(x, 2);
	value = value + 2 * coeff[2] * x + coeff[1];
	return (value);
}
