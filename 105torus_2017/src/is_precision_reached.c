/*
** EPITECH PROJECT, 2018
** is_precision_reached
** File description:
** Checks if the value is in the right precision
*/

#include <math.h>
#include "torus.h"

int	is_precision_reached(double *coefficient, int precision, double x)
{
	if (-pow(10, -precision) > calc_x(coefficient, x))
		return (0);
	if (pow(10, -precision) < calc_x(coefficient, x))
		return (0);
	return (1);
}
