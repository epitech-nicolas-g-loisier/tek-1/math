/*
** EPITECH PROJECT, 2018
** torus
** File description:
** Prototypes the functions needed in 105torus
*/

#ifndef TORUS_
#define TORUS_

void	use_newton(double *, int);

void	use_secant(double *, int);

void	use_bisection(double *, int);

int	is_precision_reached(double *, int, double);

double	calc_deriv_x(double *, double);

double	calc_x(double *, double);

int	check_precision(double, int);
#endif
