/*
** EPITECH PROJECT, 2017
** my_find_prime_sup
** File description:
** Return first prime nb after arg
*/

int	my_find_prime_sup(int nb)
{
	int div = 2;
	int first_p = nb;

	if (nb < 2)
		return (2);
	while (div < first_p) {
		if (first_p / div * div == first_p) {
			div = 2;
			first_p++;
		}
		div++;
		if (first_p == 2147483647)
			return (0);
	}
	return (first_p);
}
