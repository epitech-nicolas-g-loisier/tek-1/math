/*
** EPITECH PROJECT, 2017
** my_strupcase
** File description:
** Changes every letter in uppercase
*/

char	*my_strupcase(char *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] > 96 && str[argc] < 123)
			str[argc] = str[argc] - 32;
		argc = argc + 1;
	}
	return (str);
}
