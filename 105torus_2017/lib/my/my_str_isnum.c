/*
** EPITECH PROJECT, 2017
** my_str_isnum
** File description:
** Searches if string is num only
*/

int	my_str_isnum(char const *str)
{
	int argc = 0;

	if (str[0] == '-')
		argc = 1;
	while (str[argc] != '\0') {
		if (str[argc] >= '0' && str[argc] <= '9')
			argc = argc + 1;
		else
			return (0);
	}
	return (1);
}
