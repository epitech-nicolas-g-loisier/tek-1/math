/*
** EPITECH PROJECT, 2017
** my_print_alpha
** File description:
** Displays alphabet in correct order
*/

void	my_putchar(char c);

int	my_isneg(int n)
{
	if (n < 0)
		my_putchar(78);
	else
		my_putchar(80);
	return (0);
}
