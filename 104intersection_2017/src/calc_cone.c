/*
** EPITECH PROJECT, 2018
** 104intersection
** File description:
** Calcul intersection for cone
*/

#include <math.h>
#include <stdio.h>
#include "104intersection.h"

double	cone_find_a(vector_t dir_vect, double p)
{
	double	x_sq = dir_vect.x * dir_vect.x;
	double	y_sq = dir_vect.y * dir_vect.y;
	double	z = dir_vect.z * tan(p / 180 * M_PI);
	double	a = x_sq + y_sq - z * z;

	return (a);
}

double	cone_find_b(vector_t point_vect, vector_t dir_vect, double p)
{
	double	x = point_vect.x * dir_vect.x;
	double	y = point_vect.y * dir_vect.y;
	double	z = point_vect.z * dir_vect.z;
	double	tan_sq = tan(p / 180 * M_PI) * tan(p / 180 * M_PI);
	double	b = 2 * x + 2 * y - 2 * z * tan_sq;

	return (b);
}

double	cone_find_c(vector_t point_vect, double p)
{
	double	x = point_vect.x * point_vect.x;
	double	y = point_vect.y * point_vect.y;
	double	z = point_vect.z * point_vect.z;
	double	tan_sq = tan(p / 180 * M_PI) * tan(p / 180 * M_PI);
	double	c = x + y - z * tan_sq;

	return (c);
}

void	find_single_inter(double a, double b, vector_t p_vect, vector_t d_vect)
{
	double lambda = -b / 2 / a;

	printf("1 intersection point :\n");
	display_inter(lambda, p_vect, d_vect);
}

void	cone_intersection(vector_t point_vect, vector_t dir_vect, int p)
{
	double	a = cone_find_a(dir_vect, p);
	double	b = cone_find_b(point_vect, dir_vect, p);
	double	c = cone_find_c(point_vect, p);
	double	delta = b * b - 4 * a * c;
	double	lambda = 0;

	if ((a == 0 && b == 0) || delta < 0) {
		print_special_case(a, b, c);
	} else if (a == 0 || delta == 0) {
		if (a == 0)
			find_spe_inter(b, c, point_vect, dir_vect);
		else
			find_single_inter(a, b, point_vect, dir_vect);
	} else {
		printf("2 intersection points :\n");
		lambda = (-b + sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
		lambda = (-b - sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
	}
}
