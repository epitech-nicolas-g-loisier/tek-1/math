/*
** EPITECH PROJECT, 2018
** print_line_type
** File description:
** Prints a point and the dir of the line
*/

#include <stdio.h>
#include "104intersection.h"

void	print_line_type(vector_t p_vect, vector_t dir_vect)
{
	printf("straight line going through the (%d,%d,", p_vect.x, p_vect.y);
	printf("%d) point and of direction vector (%d,", p_vect.z, dir_vect.x);
	printf("%d,%d)\n", dir_vect.y, dir_vect.z);
}
