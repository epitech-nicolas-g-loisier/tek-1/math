/*
** EPITECH PROJECT, 2018
** print_surface_type
** File description:
** Prints the type of surface and its size
*/

#include <stdio.h>
#include "104intersection.h"

void	print_surface_type(int type, char *size)
{
	char *type_list[3] = {"sphere of ", "cylinder of ", "cone of "};

	printf("%s", type_list[type - 1]);
	if (type < 3)
		printf("radius %d\n", my_getnbr(size));
	else
		printf("%d degree angle\n", my_getnbr(size));
}
