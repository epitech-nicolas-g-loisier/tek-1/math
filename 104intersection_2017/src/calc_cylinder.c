/*
** EPITECH PROJECT, 2018
** 104intersection
** File description:
** Calcul intersection for cylinder
*/

#include <math.h>
#include <stdio.h>
#include "104intersection.h"

double	cylinder_find_a(vector_t dir_vect)
{
	double	x_sq = dir_vect.x * dir_vect.x;
	double	y_sq = dir_vect.y * dir_vect.y;
	double	a = x_sq + y_sq;

	return (a);
}

double	cylinder_find_b(vector_t point_vect, vector_t dir_vect)
{
	double	x = point_vect.x * dir_vect.x;
	double	y = point_vect.y * dir_vect.y;
	double	b = 2 * x + 2 * y;

	return (b);
}

double	cylinder_find_c(vector_t point_vect, int p)
{
	double	x = point_vect.x * point_vect.x;
	double	y = point_vect.y * point_vect.y;
	double	c = x + y - p * p;

	return (c);
}

void	print_special_case(double a, double b, double c)
{
	if (a == 0 && b == 0 && c == 0)
		printf("There is an infinite number of intersection points.\n");
	else
		printf("No intersection point.\n");
}

void	cylinder_intersection(vector_t point_vect, vector_t dir_vect, int p)
{
	double	a = cylinder_find_a(dir_vect);
	double	b = cylinder_find_b(point_vect, dir_vect);
	double	c = cylinder_find_c(point_vect, p);
	double	delta = b * b - 4 * a * c;
	double	lambda = 0;

	if ((a == 0 && b == 0) || delta < 0) {
		print_special_case(a, b, c);
	} else if (delta == 0){
		lambda = -b / 2 / a;
		printf("1 intersection point :\n");
		display_inter(lambda, point_vect, dir_vect);
	} else {
		printf("2 intersection points :\n");
		lambda = (-b + sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
		lambda = (-b - sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
	}
}
