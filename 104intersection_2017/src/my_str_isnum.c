/*
** EPITECH PROJECT, 2017
** my_str_isnum
** File description:
** Searches if string is num only
*/

int	my_str_isnum(char const *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] > 47 && str[argc] < 58)
			argc = argc + 1;
		else if (argc == 0 && str[0] == '-')
			argc = argc + 1;
		else
			return (0);
	}
	return (1);
}
