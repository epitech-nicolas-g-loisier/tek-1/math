/*
** EPITECH PROJECT, 2018
** check_inter
** File description:
** Checks if the intersection is possible and not infinite
*/

#include <unistd.h>
#include <stdlib.h>
#include "104intersection.h"

void	check_inter_sphere(vector_t d_vect)
{
	int x_sq = d_vect.x * d_vect.x;
	int y_sq = d_vect.y * d_vect.y;
	int z_sq = d_vect.z * d_vect.z;

	if (x_sq + y_sq + z_sq == 0) {
		write(2, "error: the vector does not exist\n", 33);
		exit(84);
	}
}

void	check_inter_cylinder(vector_t p_vect, vector_t d_vect)
{
	double a = cylinder_find_a(d_vect);
	double b = cylinder_find_b(p_vect, d_vect);

	if (a == 0 && b == 0 && d_vect.z == 0) {
		write(2, "error: vector is null\n", 22);
		exit(84);
	}
}

void	check_inter(int type, vector_t p_vect, vector_t d_vect)
{
	if (type == 1)
		check_inter_sphere(d_vect);
	else if (type == 2)
		check_inter_cylinder(p_vect, d_vect);
}
