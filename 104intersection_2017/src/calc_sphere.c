/*
** EPITECH PROJECT, 2018
** 104intersection
** File description:
** Calcul intersection for sphere
*/

#include <math.h>
#include <stdio.h>
#include "104intersection.h"

double	sphere_find_a(vector_t dir_vect)
{
	double	x_sq = dir_vect.x * dir_vect.x;
	double	y_sq = dir_vect.y * dir_vect.y;
	double	z_sq = dir_vect.z * dir_vect.z;
	double	a = x_sq + y_sq + z_sq;

	return (a);
}

double	sphere_find_b(vector_t point_vect, vector_t dir_vect)
{
	double	x = point_vect.x * dir_vect.x;
	double	y = point_vect.y * dir_vect.y;
	double	z = point_vect.z * dir_vect.z;
	double	b = 2 * x + 2 * y + 2 * z;

	return (b);
}

double	sphere_find_c(vector_t point_vect, int p)
{
	double	x = point_vect.x * point_vect.x;
	double	y = point_vect.y * point_vect.y;
	double	z = point_vect.z * point_vect.z;
	double	c = x + y + z - p * p;

	return (c);
}

void	display_inter(double lambda, vector_t point_vect, vector_t dir_vect)
{
	printf("(%.3f, %.3f, %.3f)\n", point_vect.x + lambda * dir_vect.x,
		point_vect.y + lambda * dir_vect.y,
		point_vect.z + lambda * dir_vect.z);
}

void	sphere_intersection(vector_t point_vect, vector_t dir_vect, int p)
{
	double	a = sphere_find_a(dir_vect);
	double	b = sphere_find_b(point_vect, dir_vect);
	double	c = sphere_find_c(point_vect, p);
	double	delta = b * b - 4 * a * c;
	double	lambda = 0;

	if (delta < 0){
		printf("No intersection point.\n");
	} else if (delta == 0){
		lambda = -b / 2 / a;
		printf("1 intersection point :\n");
		display_inter(lambda, point_vect, dir_vect);
	} else {
		printf("2 intersection points :\n");
		lambda = (-b + sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
		lambda = (-b - sqrt(delta)) / 2 / a;
		display_inter(lambda, point_vect, dir_vect);
	}
}
