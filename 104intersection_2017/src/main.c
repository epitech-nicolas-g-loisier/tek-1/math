/*
** EPITECH PROJECT, 2017
** 104intersection
** File description:
** find potential intersection points between light rays and scene objects
*/

#include <unistd.h>
#include <stdlib.h>
#include "104intersection.h"

void	print_usage(void)
{
	write(1, "Usage: ./104intersection opt xp yp zp xv yv zv p\n opt", 53);
	write(1, "\t\tnumber of the option : 1 for a sphere, 2 for a cyli", 53);
	write(1, "nder, 3 for a cone\n (xp, yp, zp)\tcoordinates of the p", 53);
	write(1, "oint by which the light ray goes through\n (xv, yv, zv)", 54);
	write(1, "\tcoordinates of the direction vector of the light ray", 53);
	write(1, "\n p\t\tparameter : radius of the sphere, radius of the", 52);
	write(1, " cylinder or angle formed by the cone and the Z-axis.\n", 54);
	exit(0);
}

void	print_err(void)
{
	write(2, "./104intersection: incorrect arg\n", 33);
	write(2, "try again with -h\n", 18);
	exit(84);
}

void	check_surface_type(char *type, char *size)
{
	if (my_getnbr(type) < 1 || my_getnbr(type) > 3)
		print_err();
	if (my_getnbr(size) == 0)
		print_err();
}

void	check_arg(int argc, char **argv)
{
	int counter = 1;

	if (argc == 2 && my_strcmp(argv[1], "-h") == 0)
		print_usage();
	if (argc != 9) {
		write(2, "./104intersection: incorrect arg count\n", 39);
		write(2, "try again with -h\n", 18);
		exit(84);
	}
	while (counter < 9) {
		if (my_str_isnum(argv[counter]) == 0)
			print_err();
		if ((counter == 1 || counter == 8) && argv[counter][0] == '-')
			print_err();
		counter = counter + 1;
	}
	check_surface_type(argv[1], argv[8]);
}

int	main(int argc, char **argv)
{
	vector_t point_vect;
	vector_t dir_vect;

	check_arg(argc, argv);
	point_vect = extract_vector(&argv[2]);
	dir_vect = extract_vector(&argv[5]);
	check_inter(my_getnbr(argv[1]), point_vect, dir_vect);
	print_surface_type(my_getnbr(argv[1]), argv[8]);
	print_line_type(point_vect, dir_vect);
	if (my_getnbr(argv[1]) == 1)
		sphere_intersection(point_vect, dir_vect, my_getnbr(argv[8]));
	else if (my_getnbr(argv[1]) == 2)
		cylinder_intersection(point_vect, dir_vect, my_getnbr(argv[8]));
	else
		cone_intersection(point_vect, dir_vect, my_getnbr(argv[8]));
	return (0);
}
