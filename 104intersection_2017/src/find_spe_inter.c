/*
** EPITECH PROJECT, 2018
** find_spe_inter
** File description:
** Finds the intersection with a cone in a special case
*/

#include <stdio.h>
#include "104intersection.h"

void	find_spe_inter(double b, double c, vector_t p_vect, vector_t d_vect)
{
	double lambda = -c / b;

	printf("1 intersection point :\n");
	display_inter(lambda, p_vect, d_vect);
}
