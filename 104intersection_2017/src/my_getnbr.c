/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** Returns a number given as a string
*/

int	char_isnum(char c)
{
	if (c < 58 && c > 47)
		return (1);
	return (0);
}

int	is_strneg(char *str)
{
	int counter = 0;
	int neg_count = 0;

	while (str[counter] != '\0' && char_isnum(str[counter]) != 1) {
		if (str[counter] == '-')
			neg_count = neg_count + 1;
		counter = counter + 1;
	}
	if (neg_count % 2 == 1)
		return (1);
	return (0);
}

int	is_overlimit(int return_v, int digc, int is_neg, int newdig)
{
	if ((digc > 9 && newdig > 2) || digc > 10)
		return (1);
	if (digc > 9 && newdig == 2) {
		if (is_neg == -1 && return_v > 147483648)
			return (1);
		if (is_neg == 1 && return_v > 147483647)
			return (1);
	}
	return (0);
}

int	my_getnbr(char *str)
{
	int return_v = 0;
	int digc = 0;
	int is_neg = 1;
	int limit_hold = 0;

	while (str[digc] > 57 || str[digc] < 48)
		digc = digc + 1;
	if (is_strneg(str) == 1)
		is_neg = -1;
	while (str[digc] < 58 && str[digc] > 47) {
		if (is_overlimit(return_v, digc, is_neg, str[digc] - 48) == 1)
			return (0);
		if (is_neg == -1 && return_v == 214748364 && str[digc] == 56)
			limit_hold = 1;
		return_v = return_v * 10 + str[digc] - 48;
		digc = digc + 1;
	}
	return (return_v * is_neg + limit_hold);
}
