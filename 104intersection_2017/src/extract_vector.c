/*
** EPITECH PROJECT, 2018
** extract_vector
** File description:
** Changes 3 str into a vector
*/

#include "104intersection.h"

vector_t	extract_vector(char **values)
{
	vector_t vector;

	vector.x = my_getnbr(values[0]);
	vector.y = my_getnbr(values[1]);
	vector.z = my_getnbr(values[2]);
	return (vector);
}
