/*
** EPITECH PROJECT, 2017
** 104intersection
** File description:
** header for 104intersection
*/

#ifndef INTERSECTION_VECTOR_
#define INTERSECTION_VECTOR_

typedef struct vector
{
	int	x;
	int	y;
	int	z;
}vector_t;

#endif

#ifndef INTERSECTION_H
#define INTERSECTION_H_

int	my_str_isnum(char *);

int	my_strcmp(char *, char *);

int	my_getnbr(char *);

double	cylinder_find_a(vector_t);

double	cylinder_find_b(vector_t, vector_t);

vector_t	extract_vector(char **);

void	print_surface_type(int, char *);

void	print_line_type(vector_t, vector_t);

void	display_inter(double, vector_t, vector_t);

void	sphere_intersection(vector_t, vector_t, int);

void	cylinder_intersection(vector_t, vector_t, int);

void	cone_intersection(vector_t, vector_t, int);

void	check_inter(int, vector_t, vector_t);

void	print_special_case(double, double, double);

void	find_spe_inter(double, double, vector_t, vector_t);

#endif /* INTERSECTION_H_ */
