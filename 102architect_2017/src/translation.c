/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** function for translation
*/

#include <unistd.h>
#include <stdlib.h>
#include "architect.h"

double	**translation(char *i_vector, char *j_vector)
{
	double	**matrice = NULL;

	matrice = malloc_matrice(matrice);
	matrice[0][0] = 1.00;
	matrice[0][1] = 0.00;
	matrice[0][2] = get_float(i_vector);
	matrice[1][0] = 0.00;
	matrice[1][1] = 1.00;
	matrice[1][2] = get_float(j_vector);
	matrice[2][0] = 0.00;
	matrice[2][1] = 0.00;
	matrice[2][2] = 1.00;
	return (matrice);
}
