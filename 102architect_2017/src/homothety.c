/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** function for homothety
*/

#include <unistd.h>
#include <stdlib.h>
#include "architect.h"

double	**homothety(char *arg1, char *arg2)
{
	double **operation = NULL;

	operation = malloc_matrice(operation);
	operation[0][0] = get_float(arg1);
	operation[0][1] = 0;
	operation[0][2] = 0;
	operation[1][0] = 0;
	operation[1][1] = get_float(arg2);
	operation[1][2] = 0;
	operation[2][0] = 0;
	operation[2][1] = 0;
	operation[2][2] = 1;
	return (operation);
}
