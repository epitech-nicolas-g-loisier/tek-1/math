/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** main for architect
*/

#include <stdio.h>
#include "architect.h"

double	**do_transfo(char **argv, int *t_count, int *count)
{
	double **matrice;
	double **(*fptr1[2])(char *, char *) = {&translation, &homothety};
	double **(*fptr2[2])(char *) = {&rotation, &symetry};

	if (*t_count < 2)
		matrice = fptr1[*t_count](argv[*count + 1], argv[*count + 2]);
	else
		matrice = fptr2[*t_count - 2](argv[*count + 1]);
	*count = *count - 1;
	*t_count = 0;
	return (matrice);
}

int	main(int argc, char **argv)
{
	int count = argc - 1;
	int t_count = 0;
	char *transfo = "thrs";
	double **t_matrice = NULL;
	double **f_matrice = NULL;

	check_syntax(argc, argv);
	while (count > 2) {
		while (argv[count][1] < 100 || argv[count][1] > 120)
			count = count - 1;
		while (argv[count][1] != transfo[t_count])
			t_count = t_count + 1;
		t_matrice = do_transfo(argv, &t_count, &count);
		if (f_matrice == NULL)
			f_matrice = t_matrice;
		else
			f_matrice = matrice_mult(f_matrice, t_matrice);
	}
	print_result(argv, argc, f_matrice);
	return (0);
}
