/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** function for symetry
*/

#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "architect.h"

double	**symetry(char *arg)
{
	double **operation = NULL;

	operation = malloc_matrice(operation);
	operation[0][0] = cos(2 * (get_float(arg) / 180 * M_PI));
	operation[0][1] = sin(2 * (get_float(arg) / 180 * M_PI));
	operation[0][2] = 0;
	operation[1][0] = sin(2 * (get_float(arg) / 180 * M_PI));
	operation[1][1] = -cos(2 * (get_float(arg) / 180 * M_PI));
	operation[1][2] = 0;
	operation[2][0] = 0;
	operation[2][1] = 0;
	operation[2][2] = 1;
	return (operation);
}
