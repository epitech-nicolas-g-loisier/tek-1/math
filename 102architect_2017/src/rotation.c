/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** function for rotation
*/

#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "architect.h"

double	**rotation(char *str)
{
	double	**matrice = NULL;
	double	angle = get_float(str);

	matrice = malloc_matrice(matrice);
	matrice[0][0] = cos(angle / 180 * M_PI);
	matrice[0][1] = -sin(angle / 180 * M_PI);
	matrice[0][2] = 0.00;
	matrice[1][0] = sin(angle / 180 * M_PI);
	matrice[1][1] = cos(angle / 180 * M_PI);
	matrice[1][2] = 0.00;
	matrice[2][0] = 0.00;
	matrice[2][1] = 0.00;
	matrice[2][2] = 1.00;
	return (matrice);
}
