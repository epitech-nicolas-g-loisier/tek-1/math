/*
** EPITECH PROJECT, 2017
** 102architect*
** File description:
** malloc or free matrice
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void	free_matrice(double **matrice)
{
	int	count = 0;

	while (count != 3) {
		free(matrice[count]);
		count++;
	}
	free(matrice);
}

double	**malloc_matrice(double **matrice)
{
	int	count = 0;

	matrice = malloc(sizeof(double *) * 3);
	while (count != 3) {
		matrice[count] = malloc(sizeof(double) * 3);
		count++;
	}
	return (matrice);
}
