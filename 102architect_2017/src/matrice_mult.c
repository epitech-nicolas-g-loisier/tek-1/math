/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** multi two matrice
*/

#include <stdlib.h>
#include "architect.h"

double	mult_ligne_columns(double *ligne, double **columns, int nb)
{
	double	result = 0;

	result = ligne[0] * columns[0][nb];
	result += ligne[1] * columns[1][nb];
	result += ligne[2] * columns[2][nb];
	if (result < 0 && result > -0.01)
		result = 0;
	return (result);
}

double	**matrice_mult(double **mat_1, double **mat_2)
{
	double	**result = NULL;

	result = malloc_matrice(result);
	result[0][0] = mult_ligne_columns(mat_1[0], mat_2, 0);
	result[0][1] = mult_ligne_columns(mat_1[0], mat_2, 1);
	result[0][2] = mult_ligne_columns(mat_1[0], mat_2, 2);
	result[1][0] = mult_ligne_columns(mat_1[1], mat_2, 0);
	result[1][1] = mult_ligne_columns(mat_1[1], mat_2, 1);
	result[1][2] = mult_ligne_columns(mat_1[1], mat_2, 2);
	result[2][0] = mult_ligne_columns(mat_1[2], mat_2, 0);
	result[2][1] = mult_ligne_columns(mat_1[2], mat_2, 1);
	result[2][2] = mult_ligne_columns(mat_1[2], mat_2, 2);
	free_matrice(mat_1);
	free_matrice(mat_2);
	return (result);
}
