/*
** EPITECH PROJECT, 2017
** get_float
** File description:
** Convert a str into a float
*/

double	get_float(char *nbr)
{
	int counter = 0;
	int neg = 1;
	double rank = 10;
	double return_v = 0;

	if (nbr[counter] == '-'){
		neg = -1;
		counter++;
	} while (nbr[counter] != '\0' && nbr[counter] != '.') {
		return_v = return_v * 10;
		return_v = return_v + nbr[counter] - '0';
		counter = counter + 1;
	} if (nbr[counter] == '.')
		counter = counter + 1;
	while (nbr[counter] != '\0') {
		return_v = return_v + (nbr[counter] - '0') / rank;
		counter = counter + 1;
		rank = rank * 10;
	}
	return ((return_v * neg));
}
