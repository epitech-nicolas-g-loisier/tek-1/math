/*
** EPITECH PROJECT, 2017
** check_syntax
** File description:
** Checks for syntax errors and exit if needed
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int	is_arg_number(char *str)
{
	int count = 0;
	int dot_counter = 0;

	if (str[0] == '-')
		count = count + 1;
	while (str[count] != '\0') {
		if ((str[count] < 48 || str[count] > 57) && str[count] != 46) {
			fprintf(stderr, "Syntax error\n");
			exit(84);
		}
		if (str[count] == '.')
			dot_counter = dot_counter + 1;
		count = count + 1;
	}
	if (dot_counter > 1 || str[0] == 46 || (str[1] == 46 && str[0] == 45)){
		fprintf(stderr, "Syntax error\n");
		exit(84);
	}
	return (0);
}

int	is_arg_empty(int counter, int argc)
{
	if (counter == argc) {
		fprintf(stderr, "Not enough arg(s)\n");
		exit(84);
	}
	return (0);
}

int	check_argv(int *counter, int argc, char **argv, char *flag)
{
	*counter = *counter + 1;
	is_arg_empty(*counter, argc);
	is_arg_number(argv[*counter]);
	if (strcmp(flag, "-t") == 0 || strcmp(flag, "-h") == 0) {
		*counter = *counter + 1;
		is_arg_empty(*counter, argc);
		is_arg_number(argv[*counter]);
	}
	*counter = *counter + 1;
	return (0);
}

int	check_argc(int argc)
{
	if (argc < 5) {
		fprintf(stderr, "Incorrect usage\n");
		fprintf(stderr, "Usage : ./102architect x y transfo1 arg11");
		fprintf(stderr, " [arg12] [transfo2 arg21 [arg22]] ...\n");
		exit(84);
	}
	return (0);
}

int	check_syntax(int argc, char **argv)
{
	int counter = 3;
	int t_count = 0;
	char *transf_list[4] = {"-t", "-h", "-r", "-s"};

	check_argc(argc);
	is_arg_number(argv[1]);
	is_arg_number(argv[2]);
	while (counter < argc) {
		if (strcmp(argv[counter], transf_list[t_count]) == 0) {
			check_argv(&counter, argc, argv, transf_list[t_count]);
			t_count = -1;
		}
		t_count = t_count + 1;
		if (t_count == 4) {
			fprintf(stderr, "Invalid transformation\n");
			fprintf(stderr, "Transf possible :\n-t\n-h\n-r\n-s\n");
			exit(84);
		}
	}
	return (counter);
}
