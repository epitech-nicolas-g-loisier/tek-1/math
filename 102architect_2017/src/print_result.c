/*
** EPITECH PROJECT, 2017
** print_result
** File description:
** Prints the result of the calcs
*/

#include <stdio.h>
#include "architect.h"

void	print_matrice(double **matrice)
{
	printf("%.2f\t%.2f\t", matrice[0][0], matrice[0][1]);
	printf("%.2f\n%.2f\t", matrice[0][2], matrice[1][0]);
	printf("%.2f\t%.2f\n", matrice[1][1], matrice[1][2]);
	printf("%.2f\t%.2f\t", matrice[2][0], matrice[2][1]);
	printf("%.2f\n", matrice[2][2]);
}

void	print_rotation(char **argv, int counter)
{
	char *str = "Symmetry about an axis inclined with an angle of";
	int nbr = get_float(argv[counter + 1]);

	if (argv[counter][1] == 'r')
		printf("Rotation at a %d degree angle\n", nbr);
	else
		printf("%s %d degrees\n", str, nbr);
}

void	print_translation(char **argv, int counter)
{
	int nbr1 = get_float(argv[counter + 1]);
	int nbr2 = get_float(argv[counter + 2]);

	if (argv[counter][1] == 't')
		printf("Translation by the vector (%d, %d)\n", nbr1, nbr2);
	else
		printf("Homothety by the ratios %d and %d\n", nbr1, nbr2);
}

void	print_result(char **argv, int argc, double **mat)
{
	int counter = 3;
	double x = get_float(argv[1]);
	double y = get_float(argv[2]);

	while (counter < argc) {
		while (argv[counter][1] < 100 || argv[counter][1] > 120)
			counter = counter + 1;
		if (argv[counter][1] == 't' || argv[counter][1] == 'h') {
			print_translation(argv, counter);
			counter = counter + 3;
		} else {
			print_rotation(argv, counter);
			counter = counter + 2;
		}
	}
	print_matrice(mat);
	printf("(%d,%d) => (", (int)x, (int)y);
	printf("%.2f,", x * mat[0][0] + y * mat[0][1] + mat[0][2]);
	printf("%.2f)\n", x * mat[1][0] + y * mat[1][1] + mat[1][2]);
	free_matrice(mat);
}
