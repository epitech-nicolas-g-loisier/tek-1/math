/*
** EPITECH PROJECT, 2017
** 102architect
** File description:
** header for 102architect
*/

#ifndef ARCHITECT_
#define ARCHITECT_

/* function prototypes */
double	get_float(char*);

double	**homothety(char*, char*);
double	**translation(char*, char*);
double	**rotation(char*);
double	**symetry(char*);

double **matrice_mult(double **, double **);

int	check_syntax(int, char **);
void	print_result(char **, int, double **);

void	free_matrice(double **);
double	**malloc_matrice(double **);
#endif
